#!/bin/bash

cd /root/gitlab-runner-config-updater/
source .venv/bin/activate
./config-updater.py --config /etc/gitlab-runner/config.toml