
Updates gitlab-runner concurrency based on number of devices  of each type online in LAVA

Install:

    pip install -r requirements.txt

Manual run:

    ./config-updater --once --config <config.toml file>


## Systemd unit file

```
cp gitlab-runner-config-updater.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable gitlab-runner-config-updater.service
```

The above assumes you are running from a checkout on `/root/`.
