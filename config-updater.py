#!/usr/bin/env python3
#
# Copyright (C) 2021 Collabora Limited
# Author: Gustavo Padovan <gustavo.padovan@collabora.com>
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice (including the next
# paragraph) shall be included in all copies or substantial portions of the
# Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

"""Query # of devices from LAVA and update gitlab-runner config.toml"""

import argparse
import json
import requests
import time
import toml

from datetime import datetime

LAVA_API="https://lava.collabora.co.uk/api/v0.2/"

CONFIG="config-example.toml"

def get_devices_data(device_type):
    retries = 30
    url =  "{}devices/?device_type={}".format(LAVA_API, device_type)

    for n in range(retries):
        try:
            response = requests.get(url)
            if response.status_code == 200:
                return json.loads(response.content.decode())['results']

            response.raise_for_status()
        except requests.exceptions.HTTPError as http_err:
            print(f'HTTP error occurred: {http_err}')
        except Exception as err:
            print(f'Other error occurred: {err}')

        time.sleep(15)

    raise SystemExit("Exceeded max retries")


def update_config(args):
    print("{}: {}".format(datetime.now(), "config update started"))

    config = args.config if args.config else CONFIG
    toml_data = toml.load(config)
    for runner in toml_data['runners']:
        device_type = runner['name'].replace('mesa-lava-collabora-', '')
        online_devices = len(list(filter(lambda x: x['health'] == "Good", get_devices_data(device_type))))
        print("{}: {} online".format(device_type, online_devices))
        runner['limit'] = online_devices + 1
        runner['request_concurrency'] = online_devices + 1
    
    toml.dump(toml_data, open(config, "w"))

    print("{}: {}".format(datetime.now(), "config update finished"))


if __name__ == '__main__':
    parser = argparse.ArgumentParser("gitlab-runner config updater")
    parser.add_argument("--config", type=str, help="gitlab-runner config.toml file path")
    parser.add_argument("--once", action='store_true', help="only run once and exit")
    parser.add_argument("--sleep", type=int, default=300, help="sleep time between updates")
    args = parser.parse_args()

    if args.once:
        update_config(args)
        exit(0)

    while True:
        update_config(args)
        time.sleep(args.sleep)
